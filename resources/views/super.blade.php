
<?php namespace
App\GeoIP\Services;
use Exception;
use Torann\GeoIP\Support\HttpClient;
use Torann\GeoIP\Services\AbstractService;
class FooBar extends AbstractService {
    /** * Http client instance. * * @var HttpClient */
    protected $client;
    /** * The "booting" method of the service. * * @return void */
    public function boot() {
        $this->client = new HttpClient([ 'base_uri' => 'http://api.foobar-ip-to-location.com/', 'query' => [ 'some_option' => $this->config('some_option'), ], ]);
        return $this->client;
    }
    /** * {@inheritdoc} */
    public function locate($ip) {
        //Get data from client
        $data = $this->client->get('find', [ 'ip' => $ip, ]);
        // Verify server response
        if ($this->client->getErrors() !== null) {
            throw new Exception('Request failed (' . $this->client->getErrors() . ')');
        }
        // Parse body content
        $json = json_decode($data[0]);
        return [ 'ip' => $ip, 'iso_code' => $json->iso_code, 'country' => $json->country, 'city' => $json->city, 'state' => $json->state, 'state_name' => $json->state_name, 'postal_code' => $json->postal_code, 'lat' => $json->lat, 'lon' => $json->lon, 'timezone' => $json->timezone, 'continent' => $json->continent, ];
    }
}
