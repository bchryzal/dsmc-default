@extends('layout')

@section('content')
    <?php  $navigo = ''; ?>
{{$navigo}}

    <!--link rel="stylesheet" href="{ { asset('js/adminlte.min.css') }}"-->

    <div style="padding-top: 16px;">
        <div class="container">

            <div class="row">
                <div class="col-md-4 mx-auto">

                    <form name="dsmc" action="{{route('accueil.store')}}" method="POST" autocomplete="off" style="padding: 16px;box-shadow: 0 8px 30px #d6dee4;text-align: center;">
                        {{ csrf_field() }}

                        <label class="mx-auto" style="color: #0c5460;text-align: center;">
                            <b> NOUS CONTACTER </b>
                        </label>

                        <div class="form-group mb-4">
                            <input class="form-control" id="event" onkeydown="return alphaonly(event);" title="Veuillez saisir que des lettres" type="text" name='nom' placeholder=" Nom " value="" required>
                            <div class="text-danger">        </div>
                        </div>

                        <div class="form-group mb-4">
                            <!--label class="col-md-4 control-label" style="color: #0c5460">Prenom</label-->
                            <input class="form-control" onkeydown="return alphaonly(event);" title="Veuillez saisir que des lettres"  type="text" name='prenom' placeholder=" Prénom " value="" required>
                            <div class="text-danger">    </div>
                        </div>


                        <div class="form-group mb-4">
                            <!--label class="col-md-4 control-label" style="color: #0c5460">Message</label-->
                            <textarea class="form-control" onkeydown="return alphaonly(event);" title="Veuillez saisir que des lettres"  type="text" name='msg' placeholder=" Message " value="" required></textarea>

                        </div>

                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='navig' value="{{$navigo}}" hidden>
                        </div>

                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='ipa' value="" hidden>
                        </div>

                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='counT' value="" hidden>
                        </div>


                        <div class="text-center">
                            <button type="submit" onclick="myFunction()" class="btn btn-info">
                                Soumettre
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <br>

    <br><br>

    <script type="text/javascript"> var mots = ras; var mot ="Salut et bienvenue chez dsmc !"; </script>

    <div class="container">
        <div class="row">
            <div class="col-md-10 mx-auto" id=pdfFonc>
                <div class="col-mb-4" style="padding: 6px;box-shadow: 0 5px 30px #d6dee4;">
                    Cliquez sur le lien de téléchargement du fichier qui vous intérresse :&nbsp;
                    <tr> <!--https://dsmc-test.herokuapp.com/admin-->
                        <td> <a href="{{route('accueil.show', $var[1])}}" target="_blank"> pdf1 &nbsp; </a> </td>

                        <td> <a href="{{route('accueil.show', $var[2])}}" target="_blank">pdf2 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[3])}}" target="_blank">pdf3 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[4])}}" target="_blank">pdf4 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[5])}}" target="_blank">pdf5 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[6])}}" target="_blank">pdf6 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[7])}}" target="_blank">pdf7 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[8])}}" target="_blank">pdf8 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[9])}}" target="_blank">pdf9 &nbsp;</a> </td>

                        <td> <a href="{{route('accueil.show', $var[10])}}" target="_blank">pdf10 </a> </td>

                    </tr>
                </div>

            </div>
        </div>
    </div>


    <div class="row col-sm-12" style="padding-top: 4px;text-align: center;">
        <div class="col-sm-10">
        </div>
        <div class="float-sm-right col-sm-2" id=myform >
            <button type="button" class="close col-md-1" onclick=closeform() data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <form name="chat" action=""  autocomplete="off" style="padding: 16px;box-shadow: 0 5px 30px #d6dee4;">
                <label id="salutation" >
                    <script type="text/javascript">
                        var mot ="Salut !!! ";
                        document.write(mot) ;
                    </script> </label>
                <div class="form-inline col-sm-12">
                    <input class="form-control col-sm-9" type="text" name="motv" value="" id="motv" >
                    <button type="button" onclick="salut()" class="btn btn-info col-sm-3"> ok </button>
                </div>

            </form>
        </div>
    </div>

     <script>
        function play(){
            var audio = document.getElementById("audio");
            audio.play();
        }

        function  alphaonly(event) {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || key == 8);
        }
    </script>
     
    <audio id="audio" src="{{ asset('js/sms.mp3') }}" ></audio>


    <script>

        var navigo = 'inconnu';

        function myFunction() {
            if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 )
            {
                navigo = 'Opera';
                document.forms["dsmc"].elements["navig"].value = "Opera";


            }
            else if(navigator.userAgent.indexOf("Chrome") != -1 )
            {
                navigo = 'Chrome'; <?php $navigo = 'Chromeoo' ?>
                document.forms["dsmc"].elements["navig"].value = "Chrome";

            }
            else if(navigator.userAgent.indexOf("Safari") != -1)
            {
                navigo = 'Safari';
                document.forms["dsmc"].elements["navig"].value = "Safari";

            }
            else if(navigator.userAgent.indexOf("Firefox") != -1 )
            {
                navigo = 'Firefox';
                document.forms["dsmc"].elements["navig"].value = "Firefox";

            }
            else if((navigator.userAgent.indexOf("netscape") != -1 ) )
            {
                navigo = 'netscape';
                document.forms["dsmc"].elements["navig"].value = "netscape";

            }else if((navigator.userAgent.indexOf("konqueror") != -1 ) )
            {
                navigo = 'konqueror';
                document.forms["dsmc"].elements["navig"].value = "konqueror";

            }else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
            {
                navigo = 'Internet Explorer';
                document.forms["dsmc"].elements["navig"].value = "Internet Explorer";

            }
            else
            {
                navigo = 'inconnu';
                document.forms["dsmc"].elements["navig"].value = "inconnu";

            }

            //alert(navigo);

        }

        document.getElementById("pdfFonc").style.display="{{$pdflink}}";

        function closeform() {
            document.getElementById("myform").style.display="none";
        }

        function salut() {

            if (document.getElementById("motv").value==="bonsoir" || document.getElementById("motv").value==="Bonsoir"
                || document.getElementById("motv").value==="BONSOIR" || document.getElementById("motv").value==="salut" ||
                document.getElementById("motv").value==="Salut"){
                document.getElementById("salutation").innerHTML="Oui, que puis-je pour vous ?";
            }else {
                document.getElementById("salutation").innerHTML="Soyez un peu claire svp!!!";
            }
            var audio = document.getElementById("audio");
            audio.play();

        }

    </script>


    <br><br>
@endsection
