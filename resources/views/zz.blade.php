@extends('layout')

@section('content')

    <div style="padding-top: 16px;">
        <div class="container">

            <div class="row">
                <div class="col-md-5 mx-auto">

                    <form name="dsmc" action="/" method="POST" autocomplete="off" style="padding: 20px;box-shadow: 0 8px 30px #d6dee4;text-align: center;">
                        <label class="mx-auto" style="color: #0c5460;text-align: center;">
                            <b> NOUS CONTACTER </b>
                        </label>

                        <div class="form-group mb-4">
                            <input class="form-control" title="Veuillez saisir votre nom ici" type="text" name='nom' placeholder=" Nom : (Ex : ZOSSOU)" value="" required>
                            <div class="text-danger">        </div>
                        </div>

                        <div class="form-group mb-4">
                            <!--label class="col-md-4 control-label" style="color: #0c5460">Prenom</label-->
                            <input class="form-control" title="Veuillez saisir votre Prénom ici"  type="text" name='prenom' placeholder=" Prénom : (Ex : Beaudelaire)" value="" required>
                            <div class="text-danger">    </div>
                        </div>


                        <div class="form-group mb-4">
                            <!--label class="col-md-4 control-label" style="color: #0c5460">Message</label-->
                            <textarea class="form-control" title="Veuillez saisir un petit text ici"  type="text" name='msg' placeholder=" Message : " value="" required></textarea>

                        </div>


                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='navig' value="" hidden>
                        </div>

                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='ipa' value="" hidden>
                        </div>

                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name='counT' value="" hidden>
                        </div>


                        <div class="text-center">
                            <button type="submit" onclick="myFunction()" class="btn btn-info">
                                Soumettre
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <br>

    <br><br>

    <script type="text/javascript"> var mots = ras; var mot ="Salut et bienvenue chez dsmc !"; </script>

    <div class="container">
        <div class="row">
            <div class="col-md-12" id=pdfFonc>
                <div class="col-mb-4" style="padding: 6px;box-shadow: 0 5px 30px #d6dee4;">
                    Cliquez sur le lien de téléchargement du fichier qui vous intérresse :
                    <tr>
                        <td> <a href="https://dsmc-test.herokuapp.com/admin" target="_blank"> pdf_1 </a> </td>

                        <td> <a href="" target="_blank">pdf_2 </a> </td>

                        <td> <a href="" target="_blank">pdf_3 </a> </td>

                        <td> <a href="" target="_blank">pdf_4 </a> </td>

                        <td> <a href="" target="_blank">pdf_5 </a> </td>

                        <td> <a href="" target="_blank">pdf_6 </a> </td>

                        <td> <a href="" target="_blank">pdf_7 </a> </td>

                        <td> <a href="" target="_blank">pdf_8 </a> </td>

                        <td> <a href="" target="_blank">pdf_9 </a> </td>

                        <td> <a href="" target="_blank">pdf_10 </a> </td>

                    </tr>
                </div>

            </div>
        </div>
    </div>


    <div class="row col-sm-12" style="padding-top: 4px;text-align: center;">
        <div class="col-sm-9">
        </div>
        <div class="col-sm-3" id=myform >
            <button type="button" class="close col-md-1" onclick=closeform() data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>

            <form name="chat" action=""  autocomplete="off" style="padding: 16px;box-shadow: 0 5px 30px #d6dee4;">
                <label id="salutation" >
                    <script type="text/javascript">
                        var mot ="Salut ! Bienvenue chez DSMC !";
                        document.write(mot) ;
                    </script> </label>
                <div class="form-inline ">
                    <input class="form-control col-sm-9" type="text" name="motv" value="" id="motv" >
                    <button type="button" onclick="salut()" class="btn btn-info col-sm-3"> ok </button>
                </div>

            </form>
        </div>
    </div>


    <script>

         document.getElementById("pdfFonc").style.display="{{$pdflink}}";

        function closeform() {
            document.getElementById("myform").style.display="none";
        }

        function salut() {

            if (document.getElementById("motv").value==="bonsoir" || document.getElementById("motv").value==="Bonsoir"
                || document.getElementById("motv").value==="BONSOIR" || document.getElementById("motv").value==="salut" ||
                document.getElementById("motv").value==="Salut"){
                document.getElementById("salutation").innerHTML="Oui, que puis-je pour vous ?";
            }else {
                document.getElementById("salutation").innerHTML="Soyez un peu claire svp!!!";
            }


        }

    </script>


    <br><br>
@endsection
