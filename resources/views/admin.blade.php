@extends('layout')

@section('content')
    <title>DSMC</title>

    <div class="container-fluid">
        <div class="row" style="text-align: center">
            <div class="pp_default col-md-12 " style="padding: 10px;box-shadow: 0 5px 30px #d6dee4; text-align: center">
                <h4> <b> La liste des infos de la base de donnée </b> </h4>
            </div>
        </div>
    </div>
    <br><br>


    <div class="card">
        <div class="card-header">
            <h3 class="card-title">La liste des visiteurs</h3>
        </div>

    <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>id</th>
                    <th>Noms</th>
                    <th>Prénoms</th>
                    <th>Messages</th>
                    <th>Adresses ip</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Region</th>
                    <th>latitude/longitude</th>
                    <th>org</th>
                    <th>Navigateurs</th>
                    <th>Dates</th>

                </tr>
                </thead>
                <tbody>
                @foreach($list as  $value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td>{{$value->nom}}</td>
                        <td>{{$value->prenom}}</td>
                        <td>{{$value->message}}</td>
                        <td>{{$value->ip}}</td>
                        <td>{{$value->pays}}</td>
                        <td>{{$value->city}}</td>
                        <td>{{$value->region}}</td>
                        <td>{{$value->loc}}</td>
                        <td>{{$value->org}}</td>
                        <td>{{$value->navigateur}}</td>
                        <td>{{$value->created_at}}</td>
                    </tr>
                @endforeach
                <tfoot>
                <tr>
                    <th>id</th>
                    <th>Noms</th>
                    <th>Prénoms</th>
                    <th>Messages</th>
                    <th>Adresses ip</th>
                    <th>Pays</th>
                    <th>Ville</th>
                    <th>Region</th>
                    <th>latitude/longitude</th>
                    <th>org</th>
                    <th>Navigateurs</th>
                    <th>Dates</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>


    <br><br>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">La liste des pdf</h3>
        </div>

        <!-- /.card-header -->
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>id visiteurs</th>
                    <th>url de pdfs</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pdfs as  $value)
                    <tr>
                        <td>{{$value->idvisiteur}}</td>
                        <td>{{$value->urlpdf}}</td>
                    </tr>
                @endforeach
                <tfoot>
                <tr>
                    <th>id visiteurs</th>
                    <th>url de pdfs</th>
                </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->



@endsection
