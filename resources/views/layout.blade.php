<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DSMC</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/admin-lte@3.0.0-rc.1/dist/css/adminlte.min.css">
    <!-- iCheck -->

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"   crossorigin="anonymous">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

    <nav class="navbar navbar-expand bg-info navbar-dark border-bottom">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item d-none d-sm-inline-block">
                <!--a href="#" class="brand-link"-->
                <img src="{{ asset('logo_dsmc.png') }}" alt="DSMC Logo" class="brand-image img-circle elevation-2"
                     style="opacity: .8">
                <!--/a-->
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <h5>DSMC | Institution</h5>
            </li>
        </ul>
        <ul class="navbar-nav mx-auto">

            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link nav-icon ">ACCUEIL<!--i class="fa fa-home"></i--> </a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link nav-icon ">NOS SERVICES<!--i class="fa fa-home"></i--> </a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link nav-icon ">QUI SOMMES NOUS ?<!--i class="fa fa-home"></i--> </a>
            </li>

            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">NOUS CONTACTER </a>
            </li>

        </ul>





    </nav>

</head>
<body class="hold-transition sidebar-mini " style="background-color: #fffff8">
<div class="wrapper">


    <!-- Navbar -->

    <!-- /.navbar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content">
        <!-- Content Header (Page header) -->

    @yield('content')

    <!-- /.content-header -->

    </div><br><br><br><br>
    <!-- /.content-wrapper -->
    <footer class=" bg-dark"><br>
        <strong class="mx-auto"> &nbsp; &nbsp;  Copyright &copy; Décembre 2019 DSMC.</strong>
        Tout droits réservés.
        <div class="float-right d-none d-sm-inline-block">
            <b>Version</b> 1.0 &nbsp; &nbsp;
        </div><br><br>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

</body>
</html>
