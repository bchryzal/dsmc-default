<?php

namespace App\Http\Controllers;

use App\oki;
use App\stockage;
use App\pdfsurl;
use App\visiteur;
use FontLib\Table\Type\name;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\DB;
use PDF;

class mainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pdflink = 'none';
        $var =  array("un", "deux", "trois", "quatre", "cinq", "six", "sept", "huite", "neuf", "dix", "onze");
      //  array_push($var, "apple", "raspberry");
        /*echo 'Nous somme le : '.date("Ymd : His");
        $temps = round(microtime(true)*1000);
       //$oki = '951';
        $path = "pdf/".round(microtime(true)*1000); echo $path;//; $path .= $temps; //$path += $oki;
        mkdir($path, 0777, true);*/

        /*$hostnamo = json_decode(file_get_contents("http://ipinfo.io/{$_SERVER['REMOTE_ADDR']}/json"));
        $hostnam = gethostbyaddr($_SERVER['REMOTE_ADDR']);

           return var_dump($hostnamo);*/
        return view('welcome', compact('pdflink','var'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /** Mon code */

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pdflink = 'block';

        /** créattion du dossier */
        $adrfolder = round(microtime(true)*1000);
        $path = "pdf/".$adrfolder;
        mkdir($path, 0777, true);

        /** génération du pdf */
       // $idFile =  $this->generationPdf($path,$request->nom, 1);

        $var =  array("0000");
        array_push($var, $this->generationPdf($path,$request->nom, 1), $this->generationPdf($path,$request->prenom, 2));
        array_push($var, $this->generationPdf($path,$request->nom, 3), $this->generationPdf($path,$request->nom, 4));
        array_push($var, $this->generationPdf($path,$request->prenom, 5), $this->generationPdf($path,$request->nom, 6));
        array_push($var, $this->generationPdf($path,$request->nom, 7), $this->generationPdf($path,$request->nom, 8));
        array_push($var, $this->generationPdf($path,$request->nom, 9), $this->generationPdf($path,$request->prenom, 10));


        /** Enrégistrement des infos du visiteur */
        $bol = $this->indo($request, $adrfolder);


        /** Enrégistrement de idvisiteur et urlpdf dans la table pdf */
        if ($bol==true){
            for ($i=1; $i<11; $i++){

                $myVar = new pdfsurl();
                $myVar->idvisiteur = $adrfolder;
                $myVar->urlpdf = $path.'/file'.$i.'.pdf';
                $myVar->save();

            }
       }



        return view('welcome', compact('pdflink',  'var'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $mva = stockage::findOrFail($id);
        $pdf = PDF::loadView('test', ['title' => $mva->data0, 'body' => $mva->data1, 'body1' => $mva->data2, 'body2' => $mva->data3, 'body3' => $mva->data4, 'body4' => $mva->data5, 'body5' => $mva->data6, 'body6' => $mva->data7, 'body7' => $mva->data8, 'body8' => $mva->data9, 'body9' => $mva->data10]);
        return $pdf->download('file.pdf');
       // $this->reconstitutionPdf($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function indos(){

        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        $hostnamo = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        $bod = "Pays (".$hostnamo->country.") Ville (".$hostnamo->city.") Region (".$hostnamo->region.") Loc (".$hostnamo->loc.") Org (".$hostnamo->org.") ";

        return $bod;

    }

    public function indo($request, $idv){

        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }


        /** Enrégistrement des infos du visiteur */
      $hostnamo = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

            $myVisit = new visiteur();
            $myVisit->id = $idv;
            $myVisit->nom = $request->nom;
            $myVisit->prenom = $request->prenom;
            $myVisit->message = $request->msg;
            $myVisit->ip = ''.$hostnamo->ip;
            $myVisit->pays = ''.$hostnamo->country;
            $myVisit->city = ''.$hostnamo->city;
            $myVisit->region = ''.$hostnamo->region;
            $myVisit->loc = ''.$hostnamo->loc;
            $myVisit->org = $hostnamo->org;
            $myVisit->navigateur = $request->navig;

            if($myVisit->save()){
                return true;
            }
                return false;


    }

    public function admin(){
        $list = DB::table('visiteurs')->get();
        $pdfs = DB::table('pdfsurls')->get();

        return view('admin', compact('list','pdfs'));
    }

    public function initdb(){

        Schema::dropIfExists('visiteurs');
        Schema::dropIfExists('stockages');
        Schema::dropIfExists('pdfsurls');


        Schema::create('visiteurs', function (Blueprint $table) {
            $table->string('id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('message');
            $table->string('ip')->default('');
            $table->string('pays')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->string('loc')->default('');
            $table->string('org')->default('');
            $table->string('navigateur')->default('');
            $table->timestamps();
        });

        Schema::create('stockages', function (Blueprint $table) {
            $table->string('id');
            $table->text('data0');
            $table->text('data1');
            $table->text('data2');
            $table->text('data3');
            $table->text('data4');
            $table->text('data5');
            $table->text('data6');
            $table->text('data7');
            $table->text('data8');
            $table->text('data9');
            $table->text('data10');

            //  $table->string('message');
            $table->timestamps();
        });

        Schema::create('pdfsurls', function (Blueprint $table) {
            $table->string('idvisiteur');
            $table->string('urlpdf')->default('');
            $table->timestamps();
        });



    }
/*
    function reconstitutionPdf($id){
        /** récupérer les données et reconstituer le pdf pour user * /
        $mva = stockage::findOrFail($id);
        $pdf = PDF::loadView('test', ['title' => $mva->data0, 'body' => $mva->data1, 'body1' => $mva->data2, 'body2' => $mva->data3, 'body3' => $mva->data4, 'body4' => $mva->data5, 'body5' => $mva->data6, 'body6' => $mva->data7, 'body7' => $mva->data8, 'body8' => $mva->data9, 'body9' => $mva->data10]);
        return $pdf->download('file.pdf');
    }
*/
    function generationPdf($adrfile, $titre, $nubfile){

        $defo =  "bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla bla ";
        $mot = 'Bonjour et bienvenue à DSMC : ';

        $var2 = array('portable ','au Bénin ','apple ','en algerie ','à credit ','à dubai ','asus ','Dell ',
            'à Paris ','de bureau ','fixe ','en ligne ','pas cher ','à abidjan ','HP ','Lenovo ','Huaewei ','Toshiba ',
            'tablette ','serveur ','32 bits ','64 bits ','avec window 7 ','avec window 8 ','avec window 10 ','8go ram ',
            'core i5 ram 8 ','core i3 ram 4go ','17 pouces ','15 pouces ','2015 ','2019 ');
        $var1 = array('acheter écran ordinateur ', 'achat pc ', ' guide d´achat ordinateur ', 'achat pc gamer ',
            'achat pc jeux HP ','acheter pc Linux ','achat pc Lenovo ');

        $rand_v1 = array_rand($var1, 2);

        $mot .= $titre ."."; //$mot .= " test000";

        $b1 = $defo; $b1 .= $var1[$rand_v1[0]] . "\n"; $b1 .= $var2[array_rand($var1, 2)[0]] . ".";

        $b2 = $defo; $b2 .= $var1[array_rand($var1, 2)[0]] . "\n"; $b2 .= $var2[$rand_v1[0]] . ".";

        $b3 = $defo; $b3 .= $var1[$rand_v1[1]] . "\n"; $b3 .= $var2[array_rand($var2, 2)[0]] . ".";

        $b4 = $defo; $b4 .= $var1[array_rand($var1, 2)[0]] . "\n"; $b4 .= $var2[$rand_v1[0]] . ".";

        $b5 = $defo; $b5 .= $var1[$rand_v1[1]] . "\n"; $b5 .= $var2[array_rand($var1, 2)[0]] . ".";

        $b6 = $defo; $b6 .= $var1[array_rand($var1, 2)[0]] . "\n"; $b6 .= $var2[$rand_v1[0]] . ".";

        $b7 = $defo; $b7 .= $var1[$rand_v1[0]] . "\n"; $b7 .= $var2[$rand_v1[1]] . ".";

        $b8 = $defo; $b8 .= $var1[array_rand($var1, 2)[0]] . "\n"; $b8 .= $var2[$rand_v1[0]] . ".";

        $b9 = $defo; $b9 .= $var1[$rand_v1[1]] . "\n"; $b9 .= $var2[$rand_v1[1]] . ".";

        $b10 = $defo; $b10 .= $var1[array_rand($var1, 2)[0]] . "\n"; $b10 .= $var2[$rand_v1[0]] . ".";

        /** Enrégistrement du fichier sur le serveur */
        $data = ['title' => $mot, 'body' => $b1, 'body1' => $b2, 'body2' => $b3, 'body3' => $b4, 'body4' => $b5, 'body5' => $b6, 'body6' => $b7, 'body7' => $b8, 'body8' => $b9, 'body9' => $b10];
        $pdf = PDF::loadView('test', $data);

        $adresse_pdf = $adrfile; $adresse_pdf .= '/file'.$nubfile.'.pdf';
        file_put_contents($adresse_pdf,$pdf->output());


        /** Stockage des données générées dans la table stockage */

        $idStock = round(microtime(true)*1000);
        $myVar = new stockage();
        $myVar->id = $idStock;
        $myVar->data0 = $mot;
        $myVar->data1 = $b1;    $myVar->data2 = $b2;    $myVar->data3 = $b3;    $myVar->data4 = $b4;    $myVar->data5 = $b5;
        $myVar->data6 = $b6;    $myVar->data7 = $b7;    $myVar->data8 = $b8;    $myVar->data9 = $b9;    $myVar->data10 = $b10;

        if ($myVar->save()){
            return $idStock;
        }

        return false;

    }





}
