<?php

namespace App\Http\Controllers;
use PDF;

App\GeoIP\Services;
use Exception;
use Torann\GeoIP\Support\HttpClient;
use Torann\GeoIP\Services\AbstractService;

use Illuminate\Http\Request;

class PdfController extends Controller
{
    /** * Http client instance. * * @var HttpClient */
    protected $client;
    /** * The "booting" method of the service. * * @return void */
    public function boot() {
        $this->client = new HttpClient([ 'base_uri' => 'http://api.foobar-ip-to-location.com/', 'query' => [ 'some_option' => $this->config('some_option'), ], ]);
        return $this->client;
    }
    /** * {@inheritdoc} */
    public function locate($ip) {
        //Get data from client
        $data = $this->client->get('find', [ 'ip' => $ip, ]);
        // Verify server response
        if ($this->client->getErrors() !== null) {
            throw new Exception('Request failed (' . $this->client->getErrors() . ')');
        }
        // Parse body content
        $json = json_decode($data[0]);
        return [ 'ip' => $ip, 'iso_code' => $json->iso_code, 'country' => $json->country, 'city' => $json->city, 'state' => $json->state, 'state_name' => $json->state_name, 'postal_code' => $json->postal_code, 'lat' => $json->lat, 'lon' => $json->lon, 'timezone' => $json->timezone, 'continent' => $json->continent, ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pdf = PDF::loadView('test', $id);

        $pdf->download('file1.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    function pdf($var){
      //  $pdf = PDF::loadView('test', $var);

       return $var->download('file1.pdf');
    }

    function pdf1($var){
        return $var->download('file1.pdf');
    }
}
