<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', function (){
    return redirect('accueil');
});

Route::resource('/accueil', 'mainController');


Route::get('/sergino', 'mainController@indos');
/*
Route::get('/p','mainController@pdf');
Route::resource('/f','PdfController');

Route::get('/fo','PdfController@pdf');

Route::get('/a','beaudctr@un');
Route::get('/b','beaudctr@deux');
Route::get('/c','beaudctr@trois');
Route::get('/d','beaudctr@quatre');
Route::get('/e','beaudctr@cinq');
Route::get('/f','beaudctr@six');
Route::get('/g','beaudctr@sept');
Route::get('/h','beaudctr@huit');
Route::get('/i','beaudctr@neuf');
Route::get('/j','beaudctr@dix');

Route::get('/be', function () {
    return view('test');
});
*/

Route::resource('/admin', 'adminController');

Route::get('/bod', 'mainController@admin');

Route::get('/initdb', 'mainController@initdb');



//https://www.youtube.com/watch?v=1USwLnnGCjI
