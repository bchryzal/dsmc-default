<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisiteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visiteurs', function (Blueprint $table) {
            $table->string('id');
            $table->string('nom');
            $table->string('prenom');
            $table->string('message');
            $table->string('ip')->default('');
            $table->string('pays')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->string('loc')->default('');
            $table->string('org')->default('');
            $table->string('navigateur')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visiteurs');
    }
}
