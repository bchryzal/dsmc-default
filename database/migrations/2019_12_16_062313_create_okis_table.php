<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOkisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('okis', function (Blueprint $table) {
            $table->string('ip')->default('');
            $table->string('country')->default('');
            $table->string('city')->default('');
            $table->string('region')->default('');
            $table->string('loc')->default('');
            $table->string('org')->default('');
            $table->string('timezone')->default('');
            $table->string('readme')->default('');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('okis');
    }
}
