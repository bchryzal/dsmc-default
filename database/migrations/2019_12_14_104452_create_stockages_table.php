<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stockages', function (Blueprint $table) {
            $table->string('id');
            $table->text('data0');
            $table->text('data1');
            $table->text('data2');
            $table->text('data3');
            $table->text('data4');
            $table->text('data5');
            $table->text('data6');
            $table->text('data7');
            $table->text('data8');
            $table->text('data9');
            $table->text('data10');

            //  $table->string('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stockages');
    }
}
